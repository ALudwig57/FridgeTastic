import React from 'react';
import { StyleSheet, View } from 'react-native';
import { colors } from './src/definitions/colors';
import Navigation from './src/Navigation/Navigation';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import {Provider} from 'react-redux';
import {store, persistor} from './src/store/config';
import { PersistGate } from 'redux-persist/integration/react';

export default function App() {
  const theme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
      ...DefaultTheme.colors,
      primary: colors.mainGreenColor,
    },
  };

  return (
    <PaperProvider theme={theme}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <View style={styles.main}>
            <Navigation />
          </View>
        </PersistGate>
      </Provider>
    </PaperProvider>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.mainGreenColor,
  },
  container: {
    flex: 1,
    paddingTop: getStatusBarHeight(),
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});
