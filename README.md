# FridgeTastic
An awesome native react app which manage your food at home. 

This app can manage your : 
- Fridge content
- Groceries
- Recipes
