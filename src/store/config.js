import {createStore, combineReducers} from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import { AsyncStorage } from 'react-native'

import recipesReducer from './reducers/recipesReducer';
import ingredientsReducer from './reducers/ingredientsReducer';
import settingsReducer from './reducers/settingsReducer'

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const appReducer = combineReducers({recipes: recipesReducer, ingredients: ingredientsReducer, settings: settingsReducer})

const rootReducer = (state, action) => {
  if (action.type === 'CLEAR_DATA') {
    state = undefined;
  }
  return appReducer(state, action);
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer);
export let persistor = persistStore(store);