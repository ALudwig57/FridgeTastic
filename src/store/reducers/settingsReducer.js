const initialCredits = 150;
const API_KEY = '925476c48e0e4dc69a38d722a9eafadf';
const NA = 'N/A';

const initialState = { autoAdd: false, autoDelete: false, apiKey: API_KEY, credits: NA, date: NA }

function saveSettings(state = initialState, action) {
    let nextState
    switch (action.type) {
        case 'SWITCH_AUTOADD':
            nextState = {
                ...state,
                autoAdd: action.value
            };
            return nextState || state
        case 'SWITCH_AUTODELETE':
            nextState = {
                ...state,
                autoDelete: action.value
            };
            return nextState || state
        case 'CHANGE_API_KEY':
            nextState = {
                ...state,
                apiKey: action.value
            };
            return nextState || state
        case 'CREDITS':
            nextState = {
                ...state,
                credits: Number((initialCredits - action.value).toFixed(2))
            };
            return nextState || state
        case 'DATE':
            nextState = {
                ...state,
                date: action.value
            };
            return nextState || state
        default:
            return state
    }
}

export default saveSettings;