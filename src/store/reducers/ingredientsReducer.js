const initialState = { list: [], fridge: [], search: "" }

function saveIngredients(state = initialState, action) {
    let nextState
    switch (action.type) {
        case 'SAVE_IN_LIST':
            nextState = {
                ...state,
                list: [...state.list, action.value]
            };
            return nextState || state
        case 'UNSAVE_FROM_LIST':
            nextState = {
                ...state,
                list: state.list.filter(ingredient => ingredient.id !== action.value.id)
            };
            return nextState || state
        case 'SORT_LIST_NAME':
            nextState = {
                ...state,
                list: state.list.slice().sort((a, b) => { return a.name.localeCompare(b.name)} )
            };
            return nextState || state
        case 'SORT_LIST_AISLE':
            nextState = {
                ...state,
                list: state.list.slice().sort((a, b) => { return a.aisle.localeCompare(b.aisle)} )
            };
            return nextState || state
        case 'SAVE_IN_FRIDGE':
            nextState = {
                ...state,
                fridge: [...state.fridge, action.value]
            };
            return nextState || state
        case 'UNSAVE_FROM_FRIDGE':
            nextState = {
                ...state,
                fridge: state.fridge.filter(id => id !== action.value)
            };
            return nextState || state
        case 'SORT_FRIDGE_NAME':
            nextState = {
                ...state,
                fridge: state.fridge.slice().sort((a, b) => { return a.name.localeCompare(b.name)} )
            };
            return nextState || state
        case 'SORT_FRIDGE_AISLE':
            nextState = {
                ...state,
                fridge: state.fridge.slice().sort((a, b) => { return a.aisle.localeCompare(b.aisle)} )
            };
            return nextState || state
        case 'SEARCH':
            nextState = {
                ...state,
                search: action.value
            };
            return nextState || state
        default:
            return state
    }
}

export default saveIngredients;