import * as React from 'react';
import { StyleSheet } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Search from '../components/Screens/Search';
import Fridge from '../components/Screens/Fridge';
import NewIng from '../components/Screens/NewIng';
import Recipe from '../components/Recipe/Recipe';
import List from '../components/Screens/List';
import Recipes from '../components/Screens/Recipes';
import Settings from '../components/Screens/Settings';
import { colors } from '../definitions/colors';
import { MaterialIcon, MaterialCommunityIcon } from '../definitions/Icon';
import TopBar from '../definitions/TopBar';

const styles = StyleSheet.create({
  icon: { backgroundColor: 'transparent' },
});

const SearchStackNavigator = createStackNavigator({
  Search: {
    screen: Search,
    navigationOptions: ({ navigation }) => ({
      title: 'Search',
      header: (<TopBar title={"Search"} button={false} back={false} settings={() => navigation.navigate("Settings")} />),
    })
  },
  Settings: {
    screen: Settings,
    navigationOptions: ({ navigation }) => ({
      title: 'Settings',
      header: (<TopBar title={"Settings"} button={false} back={true} baction={() => navigation.goBack()} />),
    })
  },
  Recipe: {
    screen: Recipe,
    navigationOptions: ({ navigation }) => ({
      title: 'Recipe',
      header: (<TopBar title={navigation.getParam('recipeName')} button={false} settings={() => navigation.navigate("Settings")} back={true} baction={() => navigation.goBack()} />),
    })
  },
})

const FridgeStackNavigator = createStackNavigator({
  Fridge: {
    screen: Fridge,
    navigationOptions: ({ navigation }) => ({
      title: 'Fridge',
      header: (<TopBar title={"My Fridge"} back={false} button={true} action={() => navigation.navigate("NewIng")} settings={() => navigation.navigate("Settings")} />),
    })
  },
  NewIng: {
    screen: NewIng,
    navigationOptions: ({ navigation }) => ({
      title: "New Ingredient",
      header: (<TopBar title={"New ingredient"} button={false} back={true} baction={() => navigation.goBack()} settings={() => navigation.navigate("Settings")} />),
    })
  },
  Settings: {
    screen: Settings,
    navigationOptions: ({ navigation }) => ({
      title: 'Settings',
      header: (<TopBar title={"Settings"} button={false} back={true} baction={() => navigation.goBack()} />),
    })
  },
})

const ListStackNavigator = createStackNavigator({
  List: {
    screen: List,
    navigationOptions: ({ navigation }) => ({
      title: 'Fridge',
      header: (<TopBar title={"My Shopping List"} back={false} button={true} action={() => navigation.navigate("NewIng")} settings={() => navigation.navigate("Settings")} />),
    })
  },
  NewIng: {
    screen: NewIng,
    navigationOptions: ({ navigation }) => ({
      title: "New Ingredient",
      header: (<TopBar title={"New ingredient"} button={false} back={true} baction={() => navigation.goBack()} settings={() => navigation.navigate("Settings")} />),
    })
  },
  Settings: {
    screen: Settings,
    navigationOptions: ({ navigation }) => ({
      title: 'Settings',
      header: (<TopBar title={"Settings"} button={false} back={true} baction={() => navigation.goBack()} />),
    })
  },
})

const RecipesStackNavigator = createStackNavigator({
  Recipes: {
    screen: Recipes,
    navigationOptions: ({ navigation }) => ({
      title: 'Recipes',
      header: (<TopBar title={"My Recipes"} button={false} back={false} settings={() => navigation.navigate("Settings")} />),
    })
  },
  Settings: {
    screen: Settings,
    navigationOptions: ({ navigation }) => ({
      title: 'Settings',
      header: (<TopBar title={"Settings"} button={false} back={true} baction={() => navigation.goBack()} />),
    })
  },
  Recipe: {
    screen: Recipe,
    navigationOptions: ({ navigation }) => ({
      title: 'Recipe',
      header: (<TopBar title={navigation.getParam('recipeName')} button={false} settings={() => navigation.navigate("Settings")} back={true} baction={() => navigation.goBack()} />),
    })
  },
})
const TabNavigator = createMaterialBottomTabNavigator(
  {
    Search: {
      screen: SearchStackNavigator,
      navigationOptions: {
        tabBarIcon: MaterialIcon('search', '#fff', 24, styles.icon),
        tabBarLabel: 'Search',
      },
    },
    Fridge: {
      screen: FridgeStackNavigator,
      navigationOptions: {
        tabBarIcon: MaterialCommunityIcon('fridge', '#fff', 24, styles.icon),
        tabBarLabel: 'My Fridge',
      },
    },
    List: {
      screen: ListStackNavigator,
      navigationOptions: {
        tabBarIcon: MaterialIcon('shopping-cart', '#fff', 24, styles.icon),
        tabBarLabel: 'My List',
      },
    },
    Recipes: {
      screen: RecipesStackNavigator,
      navigationOptions: {
        tabBarIcon: MaterialCommunityIcon('silverware-fork-knife', '#fff', 24, styles.icon),
        tabBarLabel: 'My Recipes',
      },
    }
  },
  {
    initialRouteName: 'Search',
    shifting: true,
    activeColor: '#fff',
    inactiveColor: '#fff',
    barStyle: { backgroundColor: colors.mainGreenColor },
  }
);

export default createAppContainer(TabNavigator)

