export async function getIngredients(apiKey, searchTerm, startOffset) {
    url = `https://api.spoonacular.com/food/ingredients/autocomplete?apiKey=${apiKey}&query=${searchTerm || '%20'}&metaInformation=true&number=50&offset=${startOffset || 0}`;
    console.log(url);
    try {

        const response = await fetch(url);
        if (response.ok) {
            return response;
        }

    } catch (error) {
        console.log(url);
        console.log('Error with function getIngredients' + error.message);
        throw error;
    }
}

export async function getRecipes(apiKey, searchTerm, startOffset, diet, cuisine, includeIngredients) {
    url = `https://api.spoonacular.com/recipes/complexSearch?apiKey=${apiKey}&query=${searchTerm || '%20'}&metaInformation=true&number=10&offset=${startOffset || 0}&diet=${encodeURIComponent(diet || '')}&cuisine=${encodeURIComponent(cuisine || '')}&includeIngredients=${encodeURIComponent(includeIngredients || '')}`;
    if (url === `https://api.spoonacular.com/recipes/complexSearch?apiKey=${apiKey}&query=%20&metaInformation=true&number=10&offset=0&diet=&cuisine=&includeIngredients=`)
    {
        const response = await fetch('exemple.com');
        console.log(response.json());
        if (response.ok) {
            return response;
        }
    }
    console.log(url);
    try {
        const response = await fetch(url);
        if (response.ok) {
            return response;
        }
        throw new Error(response.status);

    } catch (error) {
        console.log(url);
        console.log('Error with function getRecipes' + error.message);
        throw error;
    }
}

export async function getRecipe(apiKey, id) {
    url = `https://api.spoonacular.com/recipes/${id}/information?apiKey=${apiKey}`;
    console.log(url);
    try {
        const response = await fetch(url);
        if (response.ok) {
            return response;
        }
        throw new Error(response.status);

    } catch (error) {
        console.log(url);
        console.log('Error with function getRecipes' + error.message);
        throw error;
    }
}

export function getRecipeImage(recipeID) {
    return `https://spoonacular.com/recipeImages/` + recipeID + `-556x370.jpg`;
}

export function getIngredientImage(imagePath) {
    return "https://spoonacular.com/cdn/ingredients_100x100/" + imagePath;
}