import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, View, Keyboard } from 'react-native';
import { Searchbar, Button, Divider, Menu, ActivityIndicator } from 'react-native-paper';
import { getRecipes } from '../../API/spoonacular';
import RecipesList from '../Recipe/RecipesList';
import { connect } from 'react-redux';
import { diets, cuisines } from '../../definitions/consts';
import moment from 'moment';
import Error from '../Prez/Error';
import NoData from '../Prez/NoData';

const Search = ({ navigation, fridge, apiKey, dispatch }) => {

    const [search, setSearch] = useState("");
    const [load, setLoad] = useState();
    const [recipes, setRecipes] = useState([]);
    const [isLoading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [diet, setDiet] = useState();
    const [cuisine, setCuisine] = useState();
    const [dietMenu, setDietMenu] = useState(false);
    const [cuisineMenu, setCuisineMenu] = useState(false);
    const [fridgeSearch, setFridgeSearch] = useState(true)
    const paginationData = useRef({ currentOffset: 0, maxResults: 0 });

    let _updateSearch = search => {
        setSearch(search);
    }

    let _searchRecipes = async () => {
        paginationData.current = { currentOffset: 0, maxResults: 0 }
        _loadSearchRecipes([])
    }

    let _updateFridgeSearch = async (type) => {
        setFridgeSearch(type);
        setLoad(Math.random());
    }

    useEffect(() => {
        _searchRecipes();
    }, [fridgeSearch, load]);

    let _openDietMenu = () => setDietMenu(true);
    let _closeDietMenu = () => setDietMenu(false);

    let _openCuisineMenu = () => setCuisineMenu(true);
    let _closeCuisineMenu = () => setCuisineMenu(false);

    let _saveCredits = async (credits) => {
        const action = { type: 'CREDITS', value: credits };
        dispatch(action);
    }

    let _saveDate = async (date) => {
        const action = { type: 'DATE', value: date };
        dispatch(action);
    }

    let _loadSearchRecipes = async (prevRecipes) => {
        setError(false);
        setLoading(true);
        try {
            var searchRecipesResult = [];

            let response = fridgeSearch ? (await getRecipes(apiKey, search, paginationData.current.currentOffset, diet, cuisine)) : (await getRecipes(apiKey, "", paginationData.current.currentOffset, "", "", fridge));
            searchRecipesResult = await response.json();
            let cr = response.headers.get('x-api-quota-used');
            _saveCredits(cr);
            _saveDate(moment().format('llll'));

            paginationData.current = { currentOffset: paginationData.current.currentOffset + searchRecipesResult.number, maxResults: searchRecipesResult.totalResults }
            setRecipes([...prevRecipes, ...searchRecipesResult.results]);
        } catch (error) {
            if(error instanceof TypeError)
            {
                paginationData.current = { currentOffset: 0, maxResults: 0 }
                setRecipes([]);
            }
            else
            {
                paginationData.current = { currentOffset: 0, maxResults: 0 }
                setRecipes([]);
                setError(true);
            }
            
        } finally {
            setLoading(false);
        }
        setLoading(false);
    }

    let _loadSearchMoreRecipes = () => {
        setLoading(true);
        if (paginationData.current.currentOffset < paginationData.current.maxResults) {
            _loadSearchRecipes(recipes);
        }
        setLoading(false);
    }

    let _displayDietMenu = (list) => {
        if (list == undefined)
            return '';
        if (list.length < 1)
            return '';
        let res = [];
        list.forEach(obj => {
            res.push(<Menu.Item key={obj.id} onPress={() => {setDiet(obj.diet); _closeDietMenu()}} title={obj.diet} />)
        });
        return res;
    }

    let _displayCuisineMenu = (list) => {
        if (list == undefined)
            return '';
        if (list.length < 1)
            return '';
        let res = [];
        list.forEach(obj => {
            res.push(<Menu.Item key={obj.id} onPress={() => {setCuisine(obj.cuisine); _closeCuisineMenu()}} title={obj.cuisine} />)
        });
        return res;
    }

    return (
        <View style={styles.mainView}>
            <View style={styles.up}>
                <Searchbar
                    icon="magnify"
                    style={styles.searchField}
                    placeholder="Search..."
                    onChangeText={_updateSearch}
                    onIconPress={() => _updateFridgeSearch(true)}
                    onSubmitEditing={() => Keyboard.dismiss()}
                    value={search}
                />
                <View style={styles.two}>
                    <Menu
                        visible={dietMenu}
                        onDismiss={_closeDietMenu}
                        anchor={
                            <Button mode="contained" dark={true} uppercase={false} compact={true} style={styles.buttons} onPress={_openDietMenu}>{diet || "Diet"}</Button>
                        }>
                        {_displayDietMenu(diets)}
                    </Menu>
                    <Menu
                        visible={cuisineMenu}
                        onDismiss={_closeCuisineMenu}
                        anchor={
                            <Button mode="contained" dark={true} uppercase={false} compact={true} style={styles.buttons} onPress={_openCuisineMenu}>{cuisine || "Cuisine"}</Button>
                        }>
                        {_displayCuisineMenu(cuisines)}
                    </Menu>
                </View>
                <Divider />
                <View style={styles.one}>
                    <Button mode="contained" dark={true} uppercase={false} compact={true} style={styles.what} onPress={() => _updateFridgeSearch(false)}>Give me ideas !</Button>
                </View>
            </View>
            <Divider />
            {isLoading ?
                <ActivityIndicator style={{ padding: 20 }} animating={true} size="large" />
                :
                error ? <Error errorMessage='No data can be retrieved, try to change the key' />
                    :
                    recipes.length === 0 ? <NoData message='No recipes to show, try again !' />
                        :
                        <RecipesList
                            navigation={navigation}
                            recipes={recipes}
                            searchRecipes={_searchRecipes}
                            loadMoreRecipes={_loadSearchMoreRecipes}
                        />}
        </View>
    );
}

const mapStateToProps = (state) => {
    return {
        credits: state.settings.credits,
        date: state.settings.date,
        apiKey: state.settings.apiKey,
        fridge: state.ingredients.fridge.map(ingredient => ingredient.name).toString()
    }
}

export default connect(mapStateToProps)(Search);

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
    },
    loadingView: {
        flex: 5,
        justifyContent: 'center',
        alignItems: 'center',
        padding: '50%'
    },
    up: {
    },
    searchField: {
        padding: 5,

    },
    two: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: "space-around",
    },
    buttons: {
        width: 143,
    },
    one: {
        alignItems: 'center',
        padding: 10,
    },
    what: {
        width: '90%',
    },
    list: {
        flex: 5,
    },
    listWrapper: {
        alignItems: 'center',
    }
})