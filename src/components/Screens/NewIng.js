import React, { useState, useRef } from 'react';
import { StyleSheet, View } from 'react-native';
import { Searchbar, ActivityIndicator } from 'react-native-paper';
import IngredientsList from '../Ingredient/IngredientsList';
import { getIngredients } from '../../API/spoonacular';
import { connect } from 'react-redux';
import moment from 'moment';
import Error from '../Prez/Error';

const NewIng = ({ s, apiKey, dispatch }) => {

    const [search, setSearch] = useState(s);
    const [ingredients, setIngredients] = useState([]);
    const [error, setError] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const paginationData = useRef({ currentOffset: 0, maxResults: 0 });

    let _saveCredits = async (credits) => {
        const action = { type: 'CREDITS', value: credits };
        dispatch(action);
    }

    let _saveDate = async (date) => {
        const action = { type: 'DATE', value: date };
        dispatch(action);
    }

    let _updateSearch = search => {
        setSearch(search);
    }

    let _loadIngredients = async (prevIngredients) => {
        setLoading(true);
        setError(false);
        try {
            var searchIngredientsResult = []
            let response = (await getIngredients(apiKey, search, paginationData.current.currentOffset));
            searchIngredientsResult = await response.json();
            let cr = response.headers.get('x-api-quota-used');
            _saveCredits(cr);
            _saveDate(moment().format('llll'));

            paginationData.current = { currentOffset: paginationData.current.currentOffset + searchIngredientsResult.length, maxResults: searchIngredientsResult.totalResults }
            setIngredients([...prevIngredients, ...searchIngredientsResult]);
        } catch (error) {
            paginationData.current = { currentOffset: 0, maxResults: 0 }
            setIngredients([]);
            setError(true);
        } finally {
            setLoading(false);
        }
    }

    let _loadMoreIngredients = () => {
        if (paginationData.current.currentOffset < paginationData.current.maxResults) {
            _loadIngredients(ingredients);
        }
    }

    let _searchIngredients = async () => {
        paginationData.current = { currentOffset: 0, maxResults: 0 }
        _loadIngredients([])
    }

    let _saveInList = async (ingredient) => {
        const action = { type: 'SAVE_IN_LIST', value: ingredient };
        dispatch(action);
        return true;
    }

    let _saveInFridge = async (ingredient) => {
        const action = { type: 'SAVE_IN_FRIDGE', value: ingredient };
        dispatch(action);
        return true;
    }

    return <View style={styles.main}>
        <Searchbar
            style={styles.searchField}
            placeholder="Search..."
            onChangeText={_updateSearch}
            onSubmitEditing={_searchIngredients}
            value={search}
        />
        {isLoading ?
            <ActivityIndicator style={{ padding: 20 }} animating={true} size="large" />
            :
            error ? <Error errorMessage='No data can be retrieved' />
                :
                <IngredientsList
                    ingredients={ingredients}
                    saveInList={item => _saveInList(item)}
                    unsaveFromList={() => { }}
                    saveInFridge={item => _saveInFridge(item)}
                    unsaveFromFridge={() => { }}
                    searchIngredients={_searchIngredients}
                    loadMoreIngredients={_loadMoreIngredients}
                />}
    </View>
}

const mapStateToProps = (state) => {
    return {
        credits: state.settings.credits,
        date: state.settings.date,
        apiKey: state.settings.apiKey,
        s: state.ingredients.search
    }
}

export default connect(mapStateToProps)(NewIng);

const styles = StyleSheet.create({
    main: {
        flex: 1,
    },
    list: {
        flex: 5,
    },
    search: {
        flex: 1,
    }
})