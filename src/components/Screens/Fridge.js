import React, { useState, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { Searchbar, RadioButton, Text, Divider} from 'react-native-paper';
import IngredientsList from '../Ingredient/IngredientsList';
import { connect } from 'react-redux';
import { colors } from '../../definitions/colors';
import NoData from '../Prez/NoData';
import IngredientListScreen from '../Ingredient/IngredientListScreen';

const Fridge = ({ fridge, autoAdd, dispatch }) => {

    let _sortList = checked => {
        const action = checked ? { type: 'SORT_FRIDGE_NAME' } : { type: 'SORT_FRIDGE_AISLE' }
        dispatch(action);
    }

    let _saveInFridge = async (ingredient) => {
        const action = { type: 'SAVE_IN_FRIDGE', value: ingredient };
        dispatch(action);
        return true;
    }

    let _unsaveFromFridge = async (ingredient) => {
        let action = { type: 'UNSAVE_FROM_FRIDGE', value: ingredient };
        dispatch(action);
        if (autoAdd) {
            action = { type: 'SAVE_IN_LIST', value: ingredient };
            dispatch(action);
        }
        return false;
    }

    return <IngredientListScreen
        list={fridge}
        sortList={_sortList}
        saveInFridge={item => _saveInFridge(item)}
        unsaveFromFridge={item => _unsaveFromFridge(item)}
    />
}

const mapStateToProps = (state) => {
    return {
        fridge: state.ingredients.fridge.filter(ing => ing.name.includes(state.ingredients.search)),
        autoAdd: state.settings.autoAdd,
    }
}

export default connect(mapStateToProps)(Fridge);