import React, { useState, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { Searchbar, RadioButton, Text, Divider} from 'react-native-paper';
import IngredientsList from '../Ingredient/IngredientsList';
import { connect } from 'react-redux';
import { colors } from '../../definitions/colors';
import NoData from '../Prez/NoData';
import IngredientListScreen from '../Ingredient/IngredientListScreen';

const List = ({ list, autoDelete, dispatch }) => {


    let _sortList = checked => {
        const action = checked ? { type: 'SORT_LIST_NAME' } : { type: 'SORT_LIST_AISLE' }
        dispatch(action);
    }

    let _saveInFridge = async (ingredient) => {
        let action = { type: 'SAVE_IN_FRIDGE', value: ingredient };
        dispatch(action);
        if (autoDelete) {
            action = { type: 'UNSAVE_FROM_LIST', value: ingredient };
            dispatch(action);
        }
        return true;
    }
    let _unsaveFromFridge = async (ingredient) => {
        const action = { type: 'UNSAVE_FROM_FRIDGE', value: ingredient };
        dispatch(action);
        return false;
    }
    
    return <IngredientListScreen
        list={list}
        sortList={_sortList}
        saveInFridge={item => _saveInFridge(item)}
        unsaveFromFridge={item => _unsaveFromFridge(item)}
    />
}

const mapStateToProps = (state) => {
    return {
        list: state.ingredients.list.filter(ing => ing.name.includes(state.ingredients.search)),
        autoDelete: state.settings.autoDelete,
    }
}

export default connect(mapStateToProps)(List);