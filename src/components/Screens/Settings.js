import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Text, Switch, TextInput, Dialog, Portal, Paragraph } from 'react-native-paper';
import { colors } from '../../definitions/colors';
import { connect } from 'react-redux';

const Settings = ({ autoAdd, autoDelete, apiKey, credits, date, dispatch }) => {

    const [add, setAdd] = useState(autoAdd);
    const [del, setDel] = useState(autoDelete);
    const [key, setKey] = useState(apiKey);
    const [clear, setClear] = useState(false);

    let _showClear = () => setClear(true);
    let _hideClear = () => setClear(false);

    let _updateAdd = async (add) => {
        const action = { type: 'SWITCH_AUTOADD', value: add };
        dispatch(action);
        setAdd(add);
    }

    let _updateDel = async (del) => {
        const action = { type: 'SWITCH_AUTODELETE', value: del };
        dispatch(action);
        setDel(del);
    }
    let _updateKey = async (key) => {
        const action = { type: 'CHANGE_API_KEY', value: key };
        dispatch(action);
        setKey(key);
    }

    let _clearData = () => {
        const action = { type: 'CLEAR_DATA'};
        dispatch(action);
        setAdd(false);
        setDel(false);
        _hideClear();
    }

    return <View style={styles.main}>
        <Text style={styles.titles}>Configuration</Text>
        <View style={styles.checkboxitem}>
            <Switch
                style={styles.checkbox}
                color={colors.mainGreenColor}
                value={add}
                onValueChange={() => _updateAdd(!add)}
            />
            <Text style={styles.checkboxtext}>Add ingredients removed from the fridge to the shopping list</Text>
        </View>
        <View style={styles.checkboxitem}>
            <Switch
                style={styles.checkbox}
                color={colors.mainGreenColor}
                value={del}
                onValueChange={() => _updateDel(!del)}
            />
            <Text style={styles.checkboxtext}>When adding an ingredient to the fridge from the shopping list, remove it from the shopping list</Text>
        </View>
        <Text style={styles.titles}>API</Text>
        <Text style={styles.apitext}>API Credits remaining: {credits}</Text>
        <Text style={styles.apitext}>Last update: {date}</Text>
        <TextInput
            style={styles.apiinput}
            label='API key'
            value={key}
            onChangeText={text => _updateKey(text)}
        />
        <View style={styles.buttonWrapper}>
            <View style={{ flex: 1 }}></View>
            <Button style={styles.button} icon="delete" mode="contained" dark={true} uppercase={true} compact={true} onPress={() => _showClear()}>Clear Data</Button>
            <Portal>
                <Dialog
                    visible={clear}
                    onDismiss={() => _hideClear()}>
                    <Dialog.Title>Clear data</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Are you sure to delete all your saved lists and recipes ?</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => _hideClear()}>Cancel</Button>
                        <Button onPress={() => _clearData()}>Confirm</Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
            <View style={{ flex: 1 }}></View>
        </View>
    </View>
}

const mapStateToProps = (state) => {
    return {
        autoAdd: state.settings.autoAdd,
        autoDelete: state.settings.autoDelete,
        apiKey: state.settings.apiKey,
        credits: state.settings.credits,
        date: state.settings.date
    }
}

export default connect(mapStateToProps)(Settings);

const styles = StyleSheet.create({
    main: {
        flex: 2,
    },
    titles: {
        fontSize: 32,
        padding: 10,
    },
    checkboxitem: {
        flexDirection: 'row',
        padding: 10,
    },
    checkbox: {
        flex: 1,
    },
    checkboxtext: {
        flex: 5,
        fontSize: 18,
    },
    apitext: {
        fontSize: 18,
        padding: 10,
    },
    apiinput: {
        margin: 10,
    },
    button: {
        flex: 2,
    },
    buttonWrapper: {
        padding: 40,
        flexDirection: 'row',
    }
})