import React from 'react';
import { connect } from 'react-redux';
import RecipesList from '../Recipe/RecipesList';
import NoData from '../Prez/NoData';


const Recipes = ({ navigation, savedRecipes }) => {

    return savedRecipes.length === 0 ? <NoData message='No recipes saved yet !' />
        :
        <RecipesList
            navigation={navigation}
            recipes={savedRecipes}
            isRefreshing={false}
            searchRecipes={null}
            loadMoreRecipes={null}
        />
}

const mapStateToProps = (state) => {
    return {
        savedRecipes: state.recipes.recipes
    }
}

export default connect(mapStateToProps)(Recipes);