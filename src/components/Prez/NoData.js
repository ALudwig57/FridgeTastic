import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { colors } from '../../definitions/colors';
import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const NoData = ( {message} ) => {
  return (
    <View style={ styles.mainView }>
    <MaterialCommunityIcons name={"magnify"} size={72} color={colors.mainGreenColor}/>
      <Text style={ styles.errorText }>
        { message }
      </Text>
    </View>
  );
}

export default NoData;

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorImage: {
    width: 100,
    height: 100,
    tintColor: colors.mainGreenColor,
    marginBottom: 30,
  },
  errorText: {
    fontSize: 32,
    fontStyle:'italic',
    textAlign: 'center',
    marginBottom: 100,
  },
});