import React from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';

import RecipeItem from './RecipeItem';

const RecipesList = ({ navigation, recipes, isRefreshing, savedRecipes, searchRecipes, loadMoreRecipes }) => {

    return <FlatList
        style={styles.list}
        contentContainerStyle={styles.listWrapper}
        data={recipes}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => <RecipeItem
            recipe={item}
            click={(item) => _showRecipe(item.id)}
            navigation={navigation} />
        }
        onEndReached={loadMoreRecipes}
        onEndReachedThreshold={0.5}
        onRefresh={searchRecipes}
        refreshing={isRefreshing}
    />;
}

const mapStateToProps = (state) => {
    return {
        savedRecipes: state.recipes.recipes
    }
}

export default connect(mapStateToProps)(RecipesList);

const styles = StyleSheet.create({
    list: {
        flex: 5,
    },
    listWrapper: {
        alignItems: 'center',
    }
})