import React, { useState, useEffect } from 'react';
import { StyleSheet, ScrollView, View, Image, Text } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import { colors } from '../../definitions/colors';
import { getRecipe } from '../../API/spoonacular';
import { getIngredientImage } from '../../API/spoonacular';
import { connect } from 'react-redux';
import { IconButton } from 'react-native-paper';
import moment from 'moment';

const Recipe = ({ navigation, apiKey, savedRecipes, fridge, dispatch }) => {

    const [recipe, setRecipe] = useState(null);
    const [isLoading, setLoadingState] = useState(true);

    let recipeID = navigation.getParam('recipeID');

    useEffect(() => {
        _loadRecipe();
    }, []);

    let _loadRecipe = async () => {
        try {
            let response = await getRecipe(apiKey, recipeID);
            let cr = response.headers.get('x-api-quota-used');
            _saveCredits(cr);
            _saveDate(moment().format('llll'));
            setRecipe(await response.json());
            setLoadingState(false);
        } catch (error) {
            console.log(error);
        }
    }

    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    let _displayListsInline = (list) => {
        if (list == undefined)
            return '';
        if (list.length < 1)
            return '';
        if (list.length == 1)
            return capitalize(list[0]);
        var c = '';
        for (var i = 0; i < list.length - 1; i++) {
            c += capitalize(list[i]) + ', ';
        }
        c += capitalize(list[list.length - 1]);
        return c;
    }

    let _displayIngredients = list => {
        if (list == undefined)
            return <View></View>;
        if (list.length < 1)
            return <View></View>;
        return list.map((ingredient) => (
            <View key={ingredient.id} style={styles.ingredients}>
                <Image style={styles.imageIngredient} source={{ uri: getIngredientImage(ingredient.image) }} />
                <Text style={styles.textIngredient}>{ingredient.originalString}</Text>
            </View>
        ))
    }

    let _ingredientsInFridge = list => {
        return list.filter(a => fridge.some(b => a.id === b.id)); 
    }

    let _ingredientsNotInFridge = list => {
        return list.filter(a => !fridge.some(b => a.id === b.id)); 
    }

    let _prettyDisplayInstructions = (instructions) => {
        var res = '';
        if (instructions == undefined)
            return res;
        if (instructions.length < 1)
            return res;
        var ins = instructions[0].steps
        ins.forEach(step => {
            res += step.number + '. ' + step.step + '\n\n';
        })
        return res;
    }

    let _displayInstructions = () => {
        return <View><Text style={styles.titles}>Instructions</Text>
            <Text style={styles.text}>{
                _prettyDisplayInstructions(recipe.analyzedInstructions)
            }</Text>
        </View>
    }

    let _displayWines = () => {
        return <View>
            <Text style={styles.titles}>Some wine ?</Text>
            <Text style={styles.subtitles}>{recipe.winePairing.pairedWines.length > 0 && _displayListsInline(recipe.winePairing.pairedWines)}</Text>
            <Text style={styles.text}>{recipe.winePairing.pairingText}</Text>
        </View>
    }

    const _displayLoading = () => {
        if (isLoading) {
            return (
                <View style={styles.loadingView}>
                    <ActivityIndicator animating={true} size="large" />
                </View>
            );
        }
        return null;
    }

    const _displaySavedRecipe = () => {
        if (savedRecipes.findIndex(i => i.id === recipe.id) !== -1) {
            return (
                <IconButton
                    icon={"bookmark-remove"}
                    color={colors.mainGreenColor}
                    size={24}
                    onPress={_unsaveRecipe}
                />
            );
        }
        return (
            <IconButton
                icon={"bookmark-plus"}
                color={colors.mainGreenColor}
                size={24}
                onPress={_saveRecipe}
            />
        );
    }

    const _displayRecipe = () => {
        if (recipe) {
            return <ScrollView style={styles.main}>
                <Image style={styles.image} source={{ uri: recipe.image }} />
                <Text style={styles.bigTitle}>{recipe.title}</Text>
                {_displaySavedRecipe()}
                {recipe.cuisines.length !== 0 ? <Text style={styles.textImportant}>Cuisine(s) : {_displayListsInline(recipe.cuisines)}</Text> : <Text></Text>}
                <Text style={styles.textImportant}>Diet(s) : {_displayListsInline(recipe.diets)}</Text>
                <Text style={styles.textImportant}>Ready in {recipe.readyInMinutes} minutes, up to {recipe.servings} people</Text>
                <Text style={styles.titles}>Ingredients</Text>
                <View style={styles.lists}>
                    <View style={styles.list1}>
                        <Text style={styles.subtitles}>In my fridge</Text>
                        {_displayIngredients(_ingredientsInFridge(recipe.extendedIngredients))}
                    </View>
                    <View style={styles.list2}>
                        <Text style={styles.subtitles}>Missing</Text>
                        {_displayIngredients(_ingredientsNotInFridge(recipe.extendedIngredients))}
                    </View>
                </View>
                {console.log(recipe.analyzedInstructions === null)}
                {recipe.analyzedInstructions === null ? <Text></Text> : _displayInstructions()}
                {console.log(recipe.winePairing.pairedWines !== undefined)}
                {recipe.winePairing.pairedWines === undefined ? <Text></Text> : _displayWines()}
            </ScrollView>
        }
        return null;
    }

    let _saveCredits = async (credits) => {
        const action = { type: 'CREDITS', value: credits };
        dispatch(action);
    }

    let _saveDate = async (date) => {
        const action = { type: 'DATE', value: date };
        dispatch(action);
    }

    let _saveRecipe = async () => {
        const action = { type: 'SAVE_RECIPE', value: recipe };
        dispatch(action);
    }

    let _unsaveRecipe = async () => {
        const action = { type: 'UNSAVE_RECIPE', value: recipe.id };
        dispatch(action);
    }

    return (
        <View style={styles.main}>
            {_displayLoading()}
            {_displayRecipe()}
        </View>
    );
}

const mapStateToProps = (state) => {
    return {
        savedRecipes: state.recipes.recipes,
        credits: state.settings.credits,
        date: state.settings.date,
        fridge: state.ingredients.fridge,
        apiKey: state.settings.apiKey,
    }
}

export default connect(mapStateToProps)(Recipe);

const styles = StyleSheet.create({
    main: {
        flex: 1,
    },
    bigTitle: {
        fontSize: 32,
        fontWeight: 'bold',
        padding: 10,
    },
    titles: {
        fontSize: 28,
        fontWeight: 'bold',
        padding: 10,
    },
    subtitles: {
        fontSize: 22,
        padding: 10,
        fontWeight: 'bold',
        color: colors.mainGreenColor,
    },
    image: {
        width: '100%',
        height: 200,
    },
    text: {
        padding: 10,
    },
    textImportant: {
        padding: 10,
        fontWeight: 'bold',
        fontStyle: 'italic',
    },
    lists: {
        flexDirection: 'row',
    },
    list1: {
        flex: 1,
        borderRightWidth: 1,
    },
    list2: {
        flex: 1,
        borderLeftWidth: 1,
    },
    loadingView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: '50%'
    },
    ingredients: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    imageIngredient: {
        width: 40,
        height: 40,
        // backgroundColor: colors.mainGreenColor,
        margin: 5,
    },
    textIngredient: {
        flexWrap: 'wrap',
        width: '60%',
        margin: 5,
    }
})