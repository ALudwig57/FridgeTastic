import React from 'react';
import { View, Image, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { IconButton } from 'react-native-paper';
import { colors } from '../../definitions/colors';
import { getRecipeImage } from '../../API/spoonacular';
import { connect } from 'react-redux';

const RecipeItem = ({ navigation, recipe, savedRecipes, dispatch }) => {

    let _showRecipe = (recipeID, recipeName) => {
        navigation.navigate("Recipe", { recipeID: recipeID, recipeName: recipeName });
    }

    let _saveRecipe = async () => {
        const action = { type: 'SAVE_RECIPE', value: recipe };
        dispatch(action);
    }
    let _unsaveRecipe = async () => {
        const action = { type: 'UNSAVE_RECIPE', value: recipe.id };
        dispatch(action);
    }

    const _displaySavedRecipe = () => {
        if (savedRecipes.findIndex(i => i.id === recipe.id) !== -1) {
            return (
                <IconButton
                    icon={"bookmark-remove"}
                    color={colors.white}
                    size={24}
                    onPress={_unsaveRecipe}
                />
            );
        }
        return (
            <IconButton
                icon={"bookmark-plus"}
                color={colors.white}
                size={24}
                onPress={_saveRecipe}
            />
        );
    }

    return (
        <TouchableOpacity
            style={styles.main}
            onPress={() => _showRecipe(recipe.id, recipe.title)}
        >
            <Image style={styles.image} source={{ uri: getRecipeImage(recipe.id) }} />
            <View style={styles.bottomItem}>
                <Text style={styles.title}>{recipe.title}</Text>
                {_displaySavedRecipe()}
            </View>

        </TouchableOpacity>
    )
}

const mapStateToProps = (state) => {
    return {
        savedRecipes: state.recipes.recipes
    }
}
export default connect(mapStateToProps)(RecipeItem);

const styles = StyleSheet.create({
    main: {
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 5,
        marginRight: 5,
        paddingBottom: 5,
        alignItems: 'center',
        backgroundColor: colors.mainGreenColor,
        borderRadius: 5,
    },
    image: {
        width: "100%",
        height: 75,
        backgroundColor: 'white',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
        flexWrap: 'wrap',
        width: '80%'
    },
    bottomItem: {
        alignItems: 'center',
        flexDirection: 'row',
    }
});
