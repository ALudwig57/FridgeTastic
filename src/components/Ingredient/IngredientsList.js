import React from 'react';
import { StyleSheet, FlatList } from 'react-native';
import IngredientItem from './IngredientItem';
import { connect } from 'react-redux';

const IngredientsList = ({ ingredients, list, fridge, saveInList, unsaveFromList, saveInFridge, unsaveFromFridge, searchIngredients, loadMoreIngredients }) => {
    return <FlatList
        style={styles.list}
        data={ingredients}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => <IngredientItem
            ingredient={item}
            // inList={list.findIndex(i => i.id === item.id) !== -1}
            // inFridge={fridge.findIndex(i => i.id === item.id) !== -1}
            saveInList={saveInList}
            unsaveFromList={unsaveFromList}
            saveInFridge={saveInFridge}
            unsaveFromFridge={unsaveFromFridge}
        />}
    />
}

const mapStateToProps = (state) => {
    return {
        list: state.ingredients.list,
        fridge: state.ingredients.fridge
    }
}

export default connect(mapStateToProps)(IngredientsList);

const styles = StyleSheet.create({
    list: {
        flex: 5,
    },
    listWrapper: {
        alignItems: 'center',
    }
})