import React, { useState, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { Searchbar, RadioButton, Text, Divider} from 'react-native-paper';
import IngredientsList from '../Ingredient/IngredientsList';
import { connect } from 'react-redux';
import { colors } from '../../definitions/colors';
import NoData from '../Prez/NoData';

const IngredientListScreen = ({ list, search, sortList, saveInFridge, unsaveFromFridge, dispatch }) => {

    const [checked, setChecked] = useState(true);

    let setSearch = s => {
        const action = { type: 'SEARCH', value: s };
        dispatch(action);
    }

    let _updateSearch = search => {
        setSearch(search);
    }

    useEffect(() => {
        sortList(checked);
    }, [checked]);

    // let _sortList = () => {
    //     const action = checked ? { type: 'SORT_FRIDGE_NAME' } : { type: 'SORT_FRIDGE_AISLE' }
    //     dispatch(action);
    // }

    // let _sortList = list => {
    //     if (checked) {
    //         return list.slice().sort((a, b) => { return a.name.localeCompare(b.name)});
    //     }
    //     else {
    //         return list.slice().sort((a, b) => { return a.aisle.localeCompare(b.aisle) });
    //     }
    // }

    let _switch = check => {
        setChecked(check);
    }

    let _saveInList = async (ingredient) => {
        const action = { type: 'SAVE_IN_LIST', value: ingredient };
        dispatch(action);
        return true;
    }

    let _unsaveFromList = async (ingredient) => {
        const action = { type: 'UNSAVE_FROM_LIST', value: ingredient };
        dispatch(action);
        return false;
    }

    return list ? <View style={styles.main}>
        <View>
            <Searchbar
                style={styles.searchField}
                placeholder="Search..."
                onChangeText={_updateSearch}
                autoCapitalize='none'
                value={search}
            />
            <View style={styles.two}>
                <Text style={styles.up}>Sort by : </Text>
                <RadioButton.Android
                    value={true}
                    color={colors.mainGreenColor}
                    status={checked ? 'checked' : 'unchecked'}
                    onPress={() => _switch(true)}
                />
                <Text style={styles.up}>name</Text>
                <RadioButton.Android
                    value={false}
                    color={colors.mainGreenColor}
                    status={!checked ? 'checked' : 'unchecked'}
                    onPress={() => _switch(false)}
                />
                <Text style={styles.up}>aisle</Text>
            </View>
        </View>
        <Divider />
        {list.length === 0 ? <NoData message='No ingredients saved in your fridge yet !' />
            :
            <IngredientsList
                // ingredients={_sortList(list)}
                ingredients={list}
                saveInList={item => _saveInList(item)}
                unsaveFromList={item => _unsaveFromList(item)}
                saveInFridge={saveInFridge}
                unsaveFromFridge={unsaveFromFridge}
            />}
    </View>
        :
        null
}

const mapStateToProps = (state) => {
    return {
        search: state.ingredients.search
    }
}

export default connect(mapStateToProps)(IngredientListScreen);

const styles = StyleSheet.create({
    main: {
        flex: 1,
    },
    up: {
        fontSize: 24,
        padding: 5,
    },
    radio: {
    },
    searchField: {
        padding: 5
    },
    two: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: "space-around",
    },
    list: {
        flex: 5,
    },
    search: {
        flex: 1,
    }
})