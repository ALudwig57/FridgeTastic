import React, { useState, useEffect } from 'react';
import { View, Image, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Button } from 'react-native-paper';
import { getIngredientImage } from '../../API/spoonacular';
import { colors } from '../../definitions/colors';
import { connect } from 'react-redux';

const IngredientItem = ({ ingredient, list, fridge, saveInList, unsaveFromList, saveInFridge, unsaveFromFridge }) => {

    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    let _saveInList = async () => {
        saveInList(ingredient);
    }
    let _unsaveFromList = async () => {
        unsaveFromList(ingredient);
    }

    let _saveInFridge = async () => {
        saveInFridge(ingredient);
    }
    let _unsaveFromFridge = async () => {
        unsaveFromFridge(ingredient);
    }

    const _displaySavedInList = () => {
        if (list.findIndex(i => i.id === ingredient.id) !== -1) {
            return (
                <Button
                    mode="text"
                    compact={true}
                    onPress={_unsaveFromList}>
                    <MaterialIcons style={styles.add} name={'shopping-cart'} color={colors.mainGreenColor} size={24} />
                </Button>
            );
        }
        return (
            <Button
                mode="contained"
                compact={true}
                onPress={_saveInList}>
                <MaterialIcons style={styles.add} name={'shopping-cart'} color={colors.white} size={24} />
            </Button>
        );
    }

    const _displaySavedInFridge = () => {
        if (fridge.findIndex(i => i.id === ingredient.id) !== -1) {
            return (
                <Button
                    mode="text"
                    compact={true}
                    onPress={_unsaveFromFridge}>
                    <MaterialIcons style={styles.add} name={'delete'} color={colors.mainGreenColor} size={24} />
                </Button>
            );
        }
        return (
            <Button
                mode="contained"
                compact={true}
                onPress={_saveInFridge}>
                <MaterialCommunityIcons style={styles.add} name={'fridge'} color={colors.white} size={24} />
            </Button>
        );
    }

    return <View style={styles.main}
        onPress={() => console.log("oui")} >
        <Image style={styles.image} source={{ uri: getIngredientImage(ingredient.image) }} />
        <View style={styles.itemsContainer}>
            <Text style={styles.title}>{capitalize(ingredient.name)}</Text>
            <View style={styles.buttons}>
                {_displaySavedInList()}
                {_displaySavedInFridge()}
            </View>
        </View>
    </View>
}

const mapStateToProps = (state) => {
    return {
        list: state.ingredients.list,
        fridge: state.ingredients.fridge,
    }
}
export default connect(mapStateToProps)(IngredientItem);

const styles = StyleSheet.create({
    main: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        flexDirection: "row",
    },
    image: {
        width: 50,
        height: 50,
        backgroundColor: 'black',
    },
    itemsContainer: {
        flex: 1,
        marginLeft: 10,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
    },
    title: {
        flex: 4,
        fontSize: 20,
    },
    buttons: {
        flex: 2,
        padding: 2,
        justifyContent: "space-between",
        flexDirection: "row",
    },
    add: {
        flex: 1,
    },
    delete: {
        flex: 1,
    }
})