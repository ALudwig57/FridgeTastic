export const diets = [
    {
        "id": 1,
        "diet": "Gluten Free"
    },
    {
        "id": 2,
        "diet": "Ketogenic"
    },
    {
        "id": 3,
        "diet": "Vegetarian"
    },
    {
        "id": 4,
        "diet": "Lacto-Vegetarian"
    },
    {
        "id": 5,
        "diet": "Ovo-Vegetarian"
    },
    {
        "id": 6,
        "diet": "Vegan"
    },
    {
        "id": 7,
        "diet": "Pescetarian"
    },
    {
        "id": 8,
        "diet": "Paleo"
    },
    {
        "id": 9,
        "diet": "Primal"
    },
    {
        "id": 10,
        "diet": "Whole30"
    }
];

export const cuisines = [
    {
        "id": 1,
        "cuisine": "African"
    },
    {
        "id": 2,
        "cuisine": "American"
    },
    {
        "id": 3,
        "cuisine": "British"
    },
    {
        "id": 4,
        "cuisine": "Cajun"
    },
    {
        "id": 5,
        "cuisine": "Caribbean"
    },
    {
        "id": 6,
        "cuisine": "Chinese"
    },
    {
        "id": 7,
        "cuisine": "Eastern European"
    },
    {
        "id": 8,
        "cuisine": "European"
    },
    {
        "id": 9,
        "cuisine": "French"
    },
    {
        "id": 10,
        "cuisine": "German"
    },
    {
        "id": 11,
        "cuisine": "Greek"
    },
    {
        "id": 12,
        "cuisine": "Indian"
    },
    {
        "id": 13,
        "cuisine": "Irish"
    },
    {
        "id": 14,
        "cuisine": "Italian"
    },
    {
        "id": 15,
        "cuisine": "Japanese"
    },
    {
        "id": 16,
        "cuisine": "Jewish"
    },
    {
        "id": 17,
        "cuisine": "Korean"
    },
    {
        "id": 18,
        "cuisine": "Latin American"
    },
    {
        "id": 19,
        "cuisine": "Mediterranean"
    },
    {
        "id": 20,
        "cuisine": "Mexican"
    },
    {
        "id": 21,
        "cuisine": "Middle Eastern"
    },
    {
        "id": 22,
        "cuisine": "Nordic"
    },
    {
        "id": 23,
        "cuisine": "Southern"
    },
    {
        "id": 24,
        "cuisine": "Spanish"
    },
    {
        "id": 25,
        "cuisine": "Thai"
    },
    {
        "id": 26,
        "cuisine": "Vietnamese"
    }
];