import * as React from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const MaterialIcon = (name, color, size, style) => (
    <MaterialIcons style={style} name={name} color={color} size={size} />
);
const MaterialCommunityIcon = (name, color, size, style) => (
    <MaterialCommunityIcons style={style} name={name} color={color} size={size} />
);
export { MaterialIcon, MaterialCommunityIcon };

