import * as React from 'react';
import { Appbar, withTheme } from 'react-native-paper';
import { MaterialIcon } from './Icon';


const TopBar = ({ title, back, baction, button, action, settings }) => {
    return (
        <Appbar.Header>
            {back === true && <Appbar.BackAction color='#ffffff' onPress={baction} />}
            <Appbar.Content color='#ffffff' title={title} />
            {button === true && <Appbar.Action icon={() => (MaterialIcon('add', '#fff', 24))} color='#ffffff' onPress={action} />}
            {settings && <Appbar.Action icon={() => (MaterialIcon('more-vert', '#fff', 24))} onPress={settings} color='#ffffff' />}
        </Appbar.Header>
    );
}

export default withTheme(TopBar);